# ClickRage

## Currently Working On
1. Rest Milestone
2. Bonus pips appearing

## A personal project that uses Angular 4 & Bootstrap 3

1. The User has 8 seconds to press/click a big, red button as many times as she is able.
  1. She gains a bonus to time for clicking quickly.
2. The User can view her **countdown** counter as her time runs out.

3. The User can view her **High Score** as well as her **Current Score**

### ClickRage should utilise the following aspects of Angular 4

1. event binding
1. components
1. eventListeners _using @Input/@Output_
1. Will switch to _services_
  + Observables
1. Mobile-friendly version _not yet tested_

### Next Version

1. **High Score** and **Current Score** update as scores are achieved
1. Add _messages_ for encouragement (or discouragement)
1. **Total Clicks** changes style
  + Add _fiery_ when certain scores are reached  

## Environment Setup
1. Typescript CDN ```<script src="https://cdnjs.cloudflare.com/ajax/libs/typescript/2.6.2/typescript.min.js"></script>```
2. If using Bootstrap 3.3.7
    + Add Bootstrap to **.angular-cli.json** e.g. ```"styles": [
        "../node_modules/bootstrap/dist/css/bootstrap.css",
        "styles.css"
      ],```
    + Bootstrap CDN and add to package.json e.g. ```"dependencies": "bootstrap": "^3.3.7",```
    + Add these scripts to the bottom of **<body>** but before ***</body>***
      ```<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>```
    + Some good [references](https://loiane.com/2017/08/how-to-add-bootstrap-to-an-angular-cli-project/)
3. If using Ng-bootstrap, don't do any of \#3 but instead reference [ng-bootstrap](https://ng-bootstrap.github.io/#/getting-started)
4. Download [Angular-CLI](https://www.npmjs.com/package/angular-cli/tutorial)

## Notes
1. Changing the default HighScore can be accomplished in:
  1. *score.service* and *scores.component*
2. Changing the Countdown Timer can be accomplished in:
  1. *clicker.component* <-- although, this will eventually be contained w/in the Timer service
  2. countDownTimer CHANGED IN constructor, runBetterTimer() AND startTime + (timer * 1000)

### DarkEyesDesign © 2016-18