import { Component, EventEmitter, Output, OnInit } from '@angular/core';

import { ScoreService} from './scores/score.service';
import { SignalService} from './header/signal.service';
import { MessagesService } from './messages/messages.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ ScoreService, MessagesService]
})
export class AppComponent implements OnInit {
  lastScore: number;
  highScore: number;
  scores: {
    highScore: number,
    lastScore: number,
  };
  signalGameEnded: boolean;
  nowClicks: number;
  watchedHighScore: number;
  watchedTimer: number;
  stopStartGame: {gameStarted: boolean};
  currentPage: {};
  attempts: number;

  constructor( private scoreService: ScoreService, 
    private signalService: SignalService,
    private messagesService: MessagesService) {
    this.signalGameEnded = false;
    this.scores = {
      highScore: 0,
      lastScore: 0,
    }
    this.nowClicks = 0;
    this.watchedHighScore = 0;
    this.stopStartGame = {
      gameStarted: false
    }
    this.currentPage = this.signalService.setPage('main');
  }

  ngOnInit() {
    this.stopStartGame.gameStarted = false;
  }

}
