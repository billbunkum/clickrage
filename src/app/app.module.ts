import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ClickerComponent } from './clicker/clicker.component';
import { MessagesComponent } from './messages/messages.component';
import { ScoresComponent } from './scores/scores.component';
import { ScoreService } from './scores/score.service';
import { SignalService } from './header/signal.service';
import { MessagesService } from './messages/messages.service';
import { TimerComponent } from './timer/timer.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ClickerComponent,
    MessagesComponent,
    ScoresComponent,
    TimerComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [ScoreService, SignalService, MessagesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
