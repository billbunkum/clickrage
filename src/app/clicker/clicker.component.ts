import { Component, EventEmitter, Output, Injectable} from '@angular/core';

import { ScoreService } from '../scores/score.service';
import { MessagesService } from '../messages/messages.service';
//@Injectable() USE FOR INJECTING SERVICES INTO OTHER SERVICES
@Component({
  selector: 'app-clicker',
  templateUrl: './clicker.component.html',
  styleUrls: ['./clicker.component.css']
})
export class ClickerComponent {
  @Output() sendClicks = new EventEmitter<number>();
  @Output() onTimerFinish = new EventEmitter<{highScore: number, 
    lastScore: number}>();
  @Output() signalGameEnded = new EventEmitter<{boolean}>();
  @Output() sendAttempts = new EventEmitter<number>();
  @Output() watchHighScore = new EventEmitter<number>();
  @Output() watchTimer = new EventEmitter<number>();
  @Output() stopStartGame = new EventEmitter<{gameStarted: boolean}>();

  lastScore: number;
  highScore: number;
  totalClicks: number;
  attempts: number = 0;
  gameStarted: boolean = false;
// countDownTimer CHANGED IN constructor, 
// runBetterTimer() AND startTime + <timer * 1000>
  countDownTimer: number; 
  colorChange: boolean = false;
  bonus_time: number = 0;
  pause_rage_button: boolean = false;
  gained_bonus: boolean = false;
  game_ended_message: boolean = false;

  constructor( private scoreService: ScoreService,
      private messagesService: MessagesService) {
    this.totalClicks = 0;
    this.countDownTimer = 8;
  }

// CLICKER & START RAGING BUTTONS ARE THE SAME
  pressClicker = function($event): number {

    this.colorChange = true;
    setTimeout( () => {
      this.colorChange = false;
    }, 100);
    this.totalClicks += 1;
// PASSING clicks TO app THEN TO scores
    this.sendClicks.emit(this.totalClicks);
// CHECKING IF highScore IS REACHED, IF YES
// THEN, PASSING highScore TO app THEN TO scores
    this.highScore = this.scoreService.compareScores(this.totalClicks);
    this.watchHighScore.emit(this.highScore);

    return this.totalClicks
  }
  startRaging = function() {
    this.lastScore = 0;
    this.gameStarted = true;
// EMITS gameStarted AS true TO HEADER
    this.stopStartGame.emit({gameStarted: this.gameStarted});
// RESETS TRIGGERS w/IN scores.component
    this.signalGameEnded.emit(false);
// SHOULD REMOVE timer LOGIC AND PLACE W/IN 
// COCKPIT COMPONENT
    this.runBetterTimer();
  }
  runBetterTimer = function() {
    this.countDownTimer = 8;
    // EMITS countDownTimer
    this.watchTimer.emit(this.countDownTimer);
    this.attempts += 1;
// SEND attempts TO app
    this.sendAttempts.emit(this.attempts);
// SETS UP TIMER LOOP
    let startTime = Date.now();
    let endTime = startTime + 8000;
    let handle = setInterval( () => {
      // INCREASES startTime UNTIL IT REACHES endTime
      startTime += 1000;
      if( endTime - startTime > 0) {
        this.countDownTimer = (endTime - startTime) / 1000;
      } else {
        this.countDownTimer = 0;
      }
      // EMITS countDownTimer
      this.watchTimer.emit(this.countDownTimer);
// CALCULATES BONUS FOR SPEEDY CLICKS, OCCURS EACH 1000ms, NO NEED FOR countDownTimer
      this.bonus_time = this.scoreService.checkSpeedForBonusTime(this.totalClicks);
      if ( this.bonus_time > 0) {
        endTime += this.bonus_time;
        this.gained_bonus = this.messagesService.notifyBonusTime();
        setTimeout( () => {
          this.gained_bonus = false;
        }, 500);
      }

// ENDS TIMER LOOP & ENDS setInterval()
      if( startTime === endTime) {      
        clearInterval(handle);
        this.gameStarted = false;
        this.startRagingCoolDown();
// EMITS gameStarted AS false TO HEADER
        this.stopStartGame.emit({gameStarted: this.gameStarted});
// CALCULATE SCORES
        this.lastScore = this.scoreService.setLastScore(this.totalClicks);
        this.highScore = this.scoreService.compareScores(this.totalClicks);
// NEED TO PASS ALONG lastSCore & highScore
        this.onTimerFinish.emit({
          highScore: this.highScore, 
          lastScore: this.lastScore,
          // gameEnded: true
        });
// ALLOWS TRIGGERS w/IN scores.component
        this.signalGameEnded.emit(true);
// RESETS totalClicks
        this.totalClicks = 0;
        setTimeout( () => { 
          this.game_ended_message = this.messagesService.notifyGameEnded();
        }, 100);
      };
// REPEAT startTime & endTime COMPARISON EVERY 1000 MILLISECONDS
    }, 1000);
  }
// MOMENTARILY FREEZES rage button AFTER GAME
  startRagingCoolDown = function() {
    this.pause_rage_button = true;
    // console.log(this.pause_rage_button);
    setTimeout( () => {
      this.pause_rage_button = false;
    }, 2000);
  }

}