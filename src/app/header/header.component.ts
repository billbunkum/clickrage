import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { SignalService} from './signal.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  @Input() gameStarted: {};
  @Input() timer: number;
  currentPage: '';
  @Output() changePage = new EventEmitter<{}>();

  constructor( private signalService: SignalService) { }

// SENDS boolean TO APP TO CONVERT TO INSTRUCTIONS PAGE
  dropInstructions = function() {
  	this.currentPage = this.signalService.setPage('instructions');
  	this.changePage.emit(this.currentPage);
  };
  dropMain = function() {
  	this.currentPage = this.signalService.setPage('main');
  	this.changePage.emit(this.currentPage);
  }
}