import { Component } from '@angular/core';

export class SignalService {
  currentPage: {};

  constructor() { }

  setPage = (page) => {
    switch( page) {
      case 'main':
        this.currentPage = {
          'main': true,
          'instructions': false,
        }
        break;
      case 'instructions':
        this.currentPage = {
          'main': false,
          'instructions': true,
        }
        break;
    }
    return this.currentPage;
  }
}