import { Component, Input } from '@angular/core';

// import { MessagesService } from './messages.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent {
	// @Input() rest_timer: number;
 //  @Input() reached_milestone: boolean;
  @Input() gained_bonus: boolean = false;
  @Input() game_ended_message: boolean;
  // @Input() highscore_gained: boolean = false;
  show_bonus: boolean;

  constructor( ) {
  	// this.rest_timer = 3;
  	// this.reached_milestone = false;
  }

}
