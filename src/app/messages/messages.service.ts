import { Component, Output, EventEmitter } from '@angular/core';

export class MessagesService {
  // @Output() bonusTime = new EventEmitter<boolean>(); 
  gained_bonus: boolean = false;
  game_ended_message: boolean = false;
  highscore_message: boolean = false;

  constructor() { }

  notifyBonusTime() {
    // console.log("you got bonus time!");    
    this.gained_bonus = true;
    setTimeout( () => {
      this.gained_bonus = false;
    }, 1000);

    return this.gained_bonus;
  }

  // notifyBrokenHighScore(highscore: boolean) {
  //   this.highscore_message = highscore;
  // }

  notifyGameEnded() {
    // console.log("time over");
    this.game_ended_message = true;

    setTimeout( () => {
      this.game_ended_message = false;
    }, 500);

    return this.game_ended_message;
  }
}