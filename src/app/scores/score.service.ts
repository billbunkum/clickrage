import { Component} from '@angular/core';

export class ScoreService {
// SETS HIGH SCORE WHEN GAME STARTS  
  highScore: number = 35;
// RESETS LAST SCORE WHEN GAME STARTS
  lastScore: number = 0;
  highScoreAchieved: boolean = false;
// PERSISTS last_clicks TO COMPARE W/CURRENT clicks
  last_clicks: number = 0;

  constructor() { }

  setLastScore = function( clicks: number) {
    if( clicks) {
      this.lastScore = clicks;
    } else {
      this.lastScore = 0
    };
    return this.lastScore
  }
  compareScores = function( clicks: number) {
    if( this.highScore <= clicks) {
      this.highScore = clicks;
    } else {
      this.highScore = this.highScore
    };
    return this.highScore
  }
  checkSpeedForBonusTime = function( clicks: number) {
    let has_bonus = false;
    const bonus_time = 1000;
// bonus logic here...
    let difference = Math.abs(this.last_clicks - clicks);
    if( difference > 7) {
      has_bonus = true;
    } else {
      has_bonus = false;
    };
    this.last_clicks = clicks;
    if( has_bonus) {
      return bonus_time;
    } else {
      return 0;
    };
  }

}
