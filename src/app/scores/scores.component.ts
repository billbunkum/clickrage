import { Component, Input, Output, 
  EventEmitter, OnInit, OnChanges, SimpleChanges
} from '@angular/core';

import { ScoreService } from './score.service';
import { MessagesService } from '../messages/messages.service';
@Component({
  selector: 'app-scores',
  templateUrl: './scores.component.html',
  styleUrls: ['./scores.component.css']
})

export class ScoresComponent  {
  @Input() capturedClicks: number;
  @Input() capturedHighScore: number; 
  @Input() emittedEndGame: boolean;
  @Output() breakHighScore = new EventEmitter<{}>();

  defaultHighScore: number;
  brokenHighScore: boolean = false;
// REGULATES defaultScore OR capturedHighScore IN DOM  
  attempts: boolean;

  constructor( private messagesService: MessagesService) {
    // this.defaultHighScore = 5;
    this.defaultHighScore = 35;
  }

  turnOffFlames() {
    // console.log("turnOffFlames");
    setTimeout( () => {
      this.brokenHighScore = false;
// RESETS capturedClicks AFTER GAME OUT
      this.capturedClicks = 0;
    }, 1000);
  }

  turnOnFlames() {
    // console.log("turnOnFlames");
    this.brokenHighScore = true;
  }

  testFlames() {
    // console.log("testFlames");
    if ( this.capturedClicks === this.capturedHighScore) {
      this.turnOnFlames();
      // this.messagesService.notifyBrokenHighScore(this.brokenHighScore);
    } else {
// TURNS OFF FLAMES w/O TIMER SO MULTI. ATTEMPTS RESET PROPERLY
      this.brokenHighScore = false;
    }
  }

  defaultToCaptured() {
    // console.log("defaultToCaptured");
    if ( this.capturedHighScore) {
      this.attempts = true;
      this.breakHighScore.emit(this.attempts);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
// OCCURS ONLY ONCE GAME IS PLAYED FIRST TIME
    if ( this.attempts === false) {
      this.defaultToCaptured(); 
    }

    if ( this.capturedHighScore > this.defaultHighScore) {
      this.testFlames();
    }
    
    if ( this.emittedEndGame === true) {
      this.turnOffFlames();
      this.emittedEndGame = false;
    }
  }

  ngOnInit() {
    this.attempts = false;
  }

}
