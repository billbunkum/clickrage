import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit {
  countDownTimer: number;
  gameStarted: boolean;

  constructor() { }

  runBetterTimer = function() {
    let startTime = Date.now();
    let endTime = startTime + 5000;
    let handle = setInterval( () => {
      startTime += 1000;
      console.log("*** ", (endTime - startTime));
      if( endTime - startTime > 0) {
        this.countDownTimer = (endTime - startTime) / 1000;
      } else {
        this.countDownTimer = 0;
      }

      if( startTime === endTime) {      
        clearInterval(handle);
        this.gameStarted = false;
        // CALL W/IN scores component
        // this.calculateScores();

        console.log("Loop END");
      };

    }, 1000);
  }

  ngOnInit() {
  }

}
